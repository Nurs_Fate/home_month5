from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class Director(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return f'Режиссер: {self.name}'

    @property
    def movie_count(self):
        if hasattr(self, '_movie_count'):
            return self._movie_count
        return self.director_movie.count()

    @movie_count.setter
    def movie_count(self, value):
        self._movie_count = value

class Movie(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=300)
    duration = models.IntegerField()
    director = models.ForeignKey(Director, on_delete=models.CASCADE, related_name='director_movie')

    def __str__(self):
        return f'Фильм:{self.title}'


class Review(models.Model):
    text = models.CharField(max_length=255)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='review')
    stars = models.IntegerField(
        validators=[
            MinValueValidator(1, "Рейтинг должен быть не менее 1."),
            MaxValueValidator(5, "Рейтинг должен быть не более 5."),
        ]
    )

    def __str__(self):
        return f'Отзыв: {self.text}'
