from rest_framework import serializers
from movie_app.models import Director, Movie, Review


class DirectorSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50, required=True)

    class Meta:
        model = Director
        fields = '__all__'

    def get_movie_count(self, director):
        return director.movie_count()


class MovieSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=255, required=True)
    description = serializers.CharField(max_length=300)
    duration = serializers.IntegerField(max_value=200, min_value=1)
    director = serializers.IntegerField(required=True)

    class Meta:
        model = Movie
        fields = ('title', 'description', 'duration', 'director',)


class ReviewSerializer(serializers.ModelSerializer):
    text = serializers.CharField(max_length=255 , min_length=1)
    stars = serializers.CharField(max_length=5)
    movie = serializers.IntegerField(required=True)
    class Meta:
        model = Review
        fields = ('id', 'text', 'movie',)


class MovieReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('text',)
